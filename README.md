Streams Tutorial
================

Purpose of this tutorial is to provide an entry level introduction to the
*streams* framework. The basic examples can be run by starting the provided
streams-tutorial Jar file along with an XML configuration file of the data
flow.

For a more in-depth exploration of the streaming platform, a decent Java-IDE
with Maven support is recommended. All code examples can also be done with
a Text editor and the Apache Maven build tool.

For more information visit: [http://sfb876.de/tutorial/](http://sfb876.de/tutorial/)