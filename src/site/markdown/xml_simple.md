A Simple Setup
==============

As a first simple running example of the *TechniBall* system, we will now develop
an XML configuration which simply reads the soccer-data from a URL and executes the
`stream.soccer.GameView` processor for each item.

The `GameView` processor is a Java implemented processor that updates player and
ball positions based on processed items and displays the current positions on a
small map in a window. This essentially leads to the game being shown in a life-view.

When starting with the skeleton XML as described in [The XML Configuration](xml.html)
only a few slight changes are required:

   1. use the custom implementation of a *stream* for the soccer data and
   2. add the `GameView` processor to the executing process.

With these changes we come to the [following XML](examples/simple.xml):

      <container>
          <stream id="soccer" class="stream.io.FastSoccerStream"
                 url="http://www.jwall.org/TechniBall/game-excerpt.gz" />
                 
          <process input="soccer">
               <stream.soccer.GameView />
          </process>
      </container>
      
Again, we can directly start this XML using the *streams* runtime bundled in the
TechniBall Java archive:

     # java -cp TechniBall-1.0.jar stream.run simple.xml