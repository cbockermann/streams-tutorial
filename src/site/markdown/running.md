Running the streams-tutorial Project
====================================

The tutorial provides a combined *streams-tutorial-1.0.jar* file, that includes
all required libraries such as

  - the *streams-api*
  - the *streams-core* package
  - the *streams-runtime*
  
In addition to this, several usefule packages (e.g. *streams-plotter*) are included.  
  
The Java archive is available at

<div class="download" style="margin:auto; height: 40px; padding: 10px; margin-left: 20px;">
     <a href="http://www.sfb876.de/tutorial/streams-tutorial-1.0.jar">
     <img src="./images/download-icon.png" style="float: left; vertical-align: middle;" />
     </a>
     <div style="float: left; margin-left: 10px;">
       <div>
        <a href="/streams-tutorial-1.2.jar">streams-tutorial-1.2.jar</a>,
        Version 1.0
       </div>
       <div style="font-size: -2;">~8.0 MB, MD5 checksum: <code>c8e92f35d089612c5f6582ccd64226be</code></div>
     </div>
</div>

Along with that a sample XML is provided <a href="http://www.sfb876.de/porto/soccer-analysis.xml">here</a>:
<form style="margin: auto;">
<textarea rows="21" cols="70" readonly="readonly" class="xmlSample">
<application>
  <stream id="soccer" class="streams.tutorial.soccer.FastSoccerStream"
         url="http://www.sfb876.de/data/game.csv.gz" />
                 
  <process input="soccer">
                
    <!-- Lookup additional MetaData information -->
	<streams.tutorial.soccer.AddMetaData />
        
   	<!-- Compute player position (average of leg positions) -->
   	<streams.tutorial.soccer.AddPosition />

   	<!-- Add new feature "distance" to the data -->
   	<streams.tutorial.soccer.ComputePlayerDistance />      

            
    <!-- Start a simple display for the data -->
    <streams.tutorial.soccer.Dashboard />
        
  </process>
</application>
</textarea>
</form>

To start the example streaming application defined by this XML file, simply run the JAR archive
with the XML file as argument:

      # java -jar streams-tutorial-1.2.jar path/to/soccer-analysis.xml
     
The system will start processing the game data. The dashboard included in the XML will show up
to present the current player positions.
