# Examples for streams-video Processing

Required software for these example is a recent Java VM and
the streams-tutorial jar file, found at

  - [streams-tutorial-1.2.jar](../streams-tutorial-1.2.jar)

All examples can be started by running the jar with the provided
example configuration file, e.g.


       # java -jar streams-tutorial-1.2.jar 01_display_video.xml

## Example XML files

All the example XML files mentioned in the talk can be found
at [https://sfb876.de/data/examples/](https://sfb876.de/data/examples/).