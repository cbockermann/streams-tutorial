Getting your hands on code...
=============================

The *streams-tutorial* provides a starting point for writing custom processors and
the designing of your own streaming applications. The code is hosted in a Git
repository at:

<div style="margin: auto; text-align: center;">
   <a href="https://bitbucket.org/cbockermann/streams-tutorial/">https://bitbucket.org/cbockermann/streams-tutorial/</a>
</div>

Simply clone the repository onto your local machine by running

      git clone https://bitbucket.org/cbockermann/streams-tutorial/
      
The streams-tutorial itself is a Maven based project and can be imported into the Eclipse IDE
or compiled/packaged from the command line.

## Compiling on the command line

Command line compilation requires Apache Maven to be installed on your machine. If Maven
is installed, simply change to the directory that you cloned from the Git repository and
run `mvn package`:

     cd streams-tutorial
     mvn package
     
The resulting executable Java archive can then be found in the `target/` directory.


## Importing into eclipse

Since the most recent versions of Eclipse already support Maven projects, you can directly
import the cloned repository into your Eclipse workspace as a Maven project.