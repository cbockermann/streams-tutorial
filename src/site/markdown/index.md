Streams Tutorial
================

Purpose of this tutorial is to provide an entry level introduction to the
*streams* framework. The basic examples can be run by starting the provided
streams-tutorial Jar file along with an XML configuration file of the data
flow.

For a more in-depth exploration of the streaming platform, a decent Java-IDE
with Maven support is recommended. All code examples can also be done with
a Text editor and the Apache Maven build tool. 



A pre-compiled Java archive is available here:

<div class="download" style="margin:auto; height: 40px; padding: 10px; margin-left: 20px;">
     <a href="streams-tutorial-1.0.jar">
     <img src="./images/download-icon.png" style="float: left; vertical-align: middle;" />
     </a>
     <div style="float: left; margin-left: 10px;">
       <div>
        <a href="streams-tutorial-1.0.jar">streams-tutorial-1.0.jar</a>,
        Version 1.0
       </div>
       <div style="font-size: -2;">~8.0 MB, MD5 checksum: <code>6e28e5a378d21e0e54b10701973990bb</code></div>
     </div>
</div>


This can directly be used to start a simple example with a stream of gaussians:

<div style="margin: auto; text-align: center;">
<form style="margin: auto;">
<textarea rows="10" cols="70" readonly="readonly" style="padding: 10px; margin: auto; border: solid 1px #0a0a0; background-color: #f0f0f0; font-size: 10pt;">
<application>

   <stream id="gauss" class="stream.generator.GaussianStream"
         attributes="0.0,1.0,2.0,0.25,8.5,2.75" />

   <process input="gauss">
      <PrintData />
   </process>

</application>
</textarea>
</form> 
</div>