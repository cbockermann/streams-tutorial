The Tutorial Tour
=================

This section provides with a simple step-by-step tour through the tutorial on
using the *streams* framework. It starts with a few simple examples on using
*streams* by simply writing XML definitions.

Later sections focus on writing custom code to extend your *streams* application
with additional functionality. We use the football sensor data of the DEBS 2013
Grand Challenge for demonstration purposes here.

 
<div class="note" style="padding: 10px; margin-left: 40px; margin-right: 40px;">
   <img src="../images/work-in-progress.png" style="width: 50px; height: 50px; float:left;" />
    <p style="padding: 8px; padding-top: 0px; margin: 0px; margin-left: 80px;">
Please note that this tutorial is work in progress. We are constantly updating
and extending it with additional steps.
    </p>
</div>


A few first steps of this tutorial tour are:

  - [01 Running first Example](running.html)