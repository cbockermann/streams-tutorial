Astro-Particle Detection
========================



Getting the Java Archive
------------------------

A recent version of the *streams-tutorial* Java archive is provided here:

<div class="download" style="margin:auto; height: 40px; padding: 10px; margin-left: 20px;">
     <a href="http://www.sfb876.de/tutorial/streams-tutorial-1.0.jar">
     <img src="../images/download-icon.png" style="float: left; vertical-align: middle; padding: 8px; padding-top: 0px;" />
     </a>
     <div style="float: left; margin-left: 10px;">
       <div>
        <a href="http://www.sfb876.de/tutorial/streams-tutorial-1.1.jar">streams-tutorial-1.1.jar</a>,
        Version 1.0
       </div>
       <div style="font-size: -2;">~8.0 MB, MD5 checksum: <code>35509e290841b3695a19b1fe81a9bbe4</code></div>
     </div>
</div>

Simple right-click to download the file to your local disk or use a command line tool such as
`wget` to get the software:

       # wget http://sfb876.de/tutorial/streams-tutorial-1.1.jar



A Sample Configuration
----------------------

Next, you need to have a configuration file that defines your streaming application by means its
data flow graph. For a very simple example, we'll use generated data that uses a gaussian
random generator to produce a stream of random samples:

<div class="figure">
	<img src="../images/gauss-app.png" style="width: 300px;"/>
</div>


This simple example is defined using the following XML configuration:


    <container >

        <stream id="video"
             class="stream.io.MJpegImageStream"
               url="http://sfb876.de/tutorial/50pict_nodead.tar.gz" />

        <process input="video" >
            <stream.image.DisplayImage key="frame:image" />
            <stream.data.CreateID />

            <streams.tutorial.astro.analyze.SimpleThresholdDetection threshold="0.35"/>

            <stream.flow.If condition="%{data.detected} == 1">

              <!-- Crop image and analyze this area -->
              <streams.tutorial.astro.image.Crop output="frame:cropped"/>
            
              <stream.image.DisplayImage key="frame:cropped" />

            </stream.flow.If>

            <PrintData />

        </process>
    </container>


Starting your Application
-------------------------

To start the astro detector application defined by this XML file, simply run the JAR archive
with the XML file as argument:

      # java -jar streams-tutorial-1.1.jar particle-detector.xml