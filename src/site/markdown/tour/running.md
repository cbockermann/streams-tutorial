Running a simple Example
========================

To get you started with *streams*, all things required are an appropriate Java archive
as can be found on the [download page](../download.html) and an XML configuration file.


Getting the Java Archive
------------------------

A recent version of the *streams-tutorial* Java archive is provided here:

<div class="download" style="margin:auto; height: 40px; padding: 10px; margin-left: 20px;">
     <a href="http://www.sfb876.de/tutorial/streams-tutorial-1.0.jar">
     <img src="../images/download-icon.png" style="float: left; vertical-align: middle; padding: 8px; padding-top: 0px;" />
     </a>
     <div style="float: left; margin-left: 10px;">
       <div>
        <a href="http://www.sfb876.de/tutorial/streams-tutorial-1.0.jar">streams-tutorial-1.0.jar</a>,
        Version 1.0
       </div>
       <div style="font-size: -2;">~8.0 MB, MD5 checksum: <code>ecc3ff5a968e07c85d2fb0db65942bbd</code></div>
     </div>
</div>

Simple right-click to download the file to your local disk or use a command line tool such as
`wget` to get the software:

       # wget http://sfb876.de/tutorial/streams-tutorial-1.0.jar



A Sample Configuration
----------------------

Next, you need to have a configuration file that defines your streaming application by means its
data flow graph. For a very simple example, we'll use generated data that uses a gaussian
random generator to produce a stream of random samples:

<div class="figure">
	<img src="../images/gauss-app.png" style="width: 300px;"/>
</div>


This simple example is defined using the following XML configuration:


      <container>
          <stream id="data" class="stream.generator.GaussianStream"
                  attributes="0.0,1.0,2.0,0.25" />
                 
          <process input="data">
                <PrintData />
          </process>
      </container>

As you can see in this XML example, we used the `stream` tag to define a streaming source
called `data`. Then we added a `process` element that has its input connected to the
defined stream using the `input="data"` attribute.

Inside the `process` element, we added a simple streaming function called `PrintData` that
will simply print out the data obtained from the input to the standard output of your
terminal.


Starting your Application
-------------------------

To start the example streaming application defined by this XML file, simply run the JAR archive
with the XML file as argument:

      # java -jar streams-tutorial-1.0.jar path/to/soccer-analysis.xml
     
The system will start processing the game data. The dashboard included in the XML will show up
to present the current player positions.
