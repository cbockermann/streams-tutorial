/**
 * 
 */
package storm.example.bolts;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;

/**
 * @author chris
 * 
 */
public class CountBolt extends ExampleBolt {

	private static final long serialVersionUID = 7453464562242099247L;

	static Logger log = LoggerFactory.getLogger(CountBolt.class);
	final Map<String, Integer> counts = new HashMap<String, Integer>();

	Integer threshold = 5;

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("tag", "count"));
	}

	@Override
	public void execute(Tuple input) {

		String tag = input.getString(0);
		Integer cnt = 1;

		if (counts.containsKey(tag))
			cnt = counts.get(tag) + 1;

		counts.put(tag, cnt);

		if (cnt > threshold) {
			output.emit(asList(tag, cnt));
		}
	}

	/**
	 * @return the threshold
	 */
	public Integer getThreshold() {
		return threshold;
	}

	/**
	 * @param threshold
	 *            the threshold to set
	 */
	public void setThreshold(Integer threshold) {
		log.info("Setting threshold to {}", threshold);
		this.threshold = threshold;
	}

}
