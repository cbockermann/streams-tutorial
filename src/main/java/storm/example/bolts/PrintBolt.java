/**
 * 
 */
package storm.example.bolts;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Tuple;

/**
 * @author Christian Bockermann
 * 
 */
public class PrintBolt extends ExampleBolt {

	/** The unique class ID */
	private static final long serialVersionUID = 1742087131671148499L;

	static Logger log = LoggerFactory.getLogger(PrintBolt.class);

	/**
	 * @see backtype.storm.task.IBolt#execute(backtype.storm.tuple.Tuple)
	 */
	@Override
	public void execute(Tuple input) {

		StringBuffer s = new StringBuffer("[");
		for (int i = 0; i < input.size(); i++) {
			Object value = input.getValue(i);
			if (value == null) {
				value = "null";
			}
			s.append(value);
			if (i + 1 < input.size()) {
				s.append(", ");
			}
		}
		s.append("]");

		log.info("{}", s.toString());
	}

	/**
	 * @see backtype.storm.topology.IComponent#declareOutputFields(backtype.storm.topology.OutputFieldsDeclarer)
	 */
	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
	}
}
