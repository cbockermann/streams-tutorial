/**
 * 
 */
package storm.example.bolts;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.base.BaseRichBolt;

/**
 * @author Christian Bockermann
 * 
 */
public abstract class ExampleBolt extends BaseRichBolt {

	/** The unique class ID */
	private static final long serialVersionUID = -7923233864552144494L;

	protected OutputCollector output;

	/**
	 * @see backtype.storm.task.IBolt#prepare(java.util.Map,
	 *      backtype.storm.task.TopologyContext,
	 *      backtype.storm.task.OutputCollector)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public void prepare(Map stormConf, TopologyContext context,
			OutputCollector collector) {
		output = collector;
	}

	public List<Object> asList(Object tag) {
		List<Object> obs = new ArrayList<Object>();
		obs.add(tag);
		return obs;
	}

	public List<Object> asList(Object tag, Object count) {
		List<Object> obs = new ArrayList<Object>();
		obs.add(tag);
		obs.add(count);
		return obs;
	}
}
