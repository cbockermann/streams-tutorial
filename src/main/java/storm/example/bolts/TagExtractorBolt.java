/**
 * 
 */
package storm.example.bolts;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;

/**
 * @author Christian Bockermann
 */
public class TagExtractorBolt extends ExampleBolt {

	private static final long serialVersionUID = 5847279231931627762L;

	static Logger log = LoggerFactory.getLogger(TagExtractorBolt.class);

	/**
	 * @see backtype.storm.task.IBolt#execute(backtype.storm.tuple.Tuple)
	 */
	@Override
	public void execute(Tuple input) {
		String tweet = input.getString(0);

		for (String token : tweet.split("\\s+")) {
			if (token.startsWith("#")) {
				log.debug("Found tag '{}'", token);
				List<Object> tag = asList(token.toLowerCase());
				output.emit(tag);
			}
		}
	}

	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("tag"));
	}

}
