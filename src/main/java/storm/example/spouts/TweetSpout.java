/**
 * 
 */
package storm.example.spouts;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;
import stream.io.JSONStream;
import stream.io.SourceURL;
import stream.io.Stream;
import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;

/**
 * @author Christian Bockermann
 * 
 */
public class TweetSpout extends BaseRichSpout {

	/** The unique class ID */
	private static final long serialVersionUID = 1205461826528679952L;

	static Logger log = LoggerFactory.getLogger(TweetSpout.class);

	Stream stream;
	SpoutOutputCollector output;

	/**
	 * @see backtype.storm.spout.ISpout#open(java.util.Map,
	 *      backtype.storm.task.TopologyContext,
	 *      backtype.storm.spout.SpoutOutputCollector)
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public void open(Map conf, TopologyContext context,
			SpoutOutputCollector collector) {

		this.output = collector;

		try {
			SourceURL url = new SourceURL(
					"http://download.jwall.org/streams/sample-tweets.json.gz");
			stream = new JSONStream(url);

			stream.init();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see backtype.storm.spout.ISpout#nextTuple()
	 */
	@Override
	public void nextTuple() {
		try {
			Data item = stream.read();
			// log.info("Item read: {}", item);

			if (item != null) {
				String tweet = item.get("text") + "";
				// log.info("Emitting: '{}'", tweet);
				List<Object> out = new ArrayList<Object>();
				out.add(tweet);
				output.emit(out);
			} else {
				log.info("Failed to read item...");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @see backtype.storm.topology.IComponent#declareOutputFields(backtype.storm
	 *      .topology.OutputFieldsDeclarer)
	 */
	@Override
	public void declareOutputFields(OutputFieldsDeclarer declarer) {
		declarer.declare(new Fields("tweet"));
	}
}