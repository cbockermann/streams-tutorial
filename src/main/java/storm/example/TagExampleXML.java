/**
 * 
 */
package storm.example;

import java.net.URL;

/**
 * @author chris
 * 
 */
public class TagExampleXML {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {

		try {
			URL url = TagExampleXML.class
					.getResource("/examples/G_storm/tag-counting.xml");
			storm.run.main(url, 10 * 1000L);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
