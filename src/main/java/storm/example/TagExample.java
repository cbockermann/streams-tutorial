/**
 * 
 */
package storm.example;

import java.util.HashMap;
import java.util.Map;

import storm.example.bolts.CountBolt;
import storm.example.bolts.PrintBolt;
import storm.example.bolts.TagExtractorBolt;
import storm.example.spouts.TweetSpout;
import backtype.storm.LocalCluster;
import backtype.storm.generated.StormTopology;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.tuple.Fields;

/**
 * @author chris
 * 
 */
public class TagExample {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {

		Map<String, String> config = new HashMap<String, String>();

		LocalCluster cluster = new LocalCluster();
		TopologyBuilder builder = new TopologyBuilder();

		builder.setSpout("tweets", new TweetSpout());

		builder.setBolt("tags", new TagExtractorBolt(), 2).shuffleGrouping(
				"tweets");

		builder.setBolt("counts", new CountBolt(), 4).fieldsGrouping("tags",
				new Fields("tag"));

		builder.setBolt("output", new PrintBolt()).allGrouping("counts");

		StormTopology graph = builder.createTopology();

		cluster.submitTopology("example", config, graph);

		Thread.sleep(10000);

		cluster.killTopology("example");
	}
}
