/**
 * 
 */
package test;

import java.io.File;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author chris
 * 
 */
public class MoaCoffeeTesting {
	static Logger log = LoggerFactory.getLogger(MoaCoffeeTesting.class);

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		File file = new File("coffee-testing.xml");
		URL url = new URL("file:" + file.getAbsolutePath());
		log.info("Starting from file {}", url);
		stream.run.main(url);
	}

}
