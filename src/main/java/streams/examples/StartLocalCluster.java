package streams.examples;

import java.net.URL;

/**
 * @author Hendrik Blom
 *
 */
public class StartLocalCluster {

	public static void main(String[] args) throws Exception {

		URL url = StartLocalCluster.class
		// .getResource("/examples/A_sources/04_json_stream.xml");
				.getResource("/examples/B_sinks/02_tcpip_central.xml");
		storm.run.main(url);

	}
}
