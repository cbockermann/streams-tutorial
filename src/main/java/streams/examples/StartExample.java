package streams.examples;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import stream.runtime.Container;
import stream.runtime.Containers;

/**
 * @author Hendrik Blom
 *
 */
public class StartExample {

	public static Map<String, String> getExamples() {
		Map<String, String> ex = new HashMap<String, String>();

		// A_sources
		ex.put("csv_stream", "/examples/A_sources/01_csv_stream.xml");
		ex.put("rnd_stream", "/examples/A_sources/02_rnd_stream.xml");
		ex.put("video_stream", "/examples/A_sources/03_video_stream.xml");
		ex.put("json_stream", "/examples/A_sources/04_json_stream.xml");

		// B_sinks
		ex.put("csv_writer", "/examples/B_sinks/01_csv_writer.xml");

		// C_flow
		ex.put("emitter", "/examples/C_flow/01_emitter.xml");
		ex.put("queue", "/examples/C_flow/02_queue.xml");
		ex.put("queue_alt", "/examples/C_flow/03_queue_alt.xml");
		ex.put("if", "/examples/C_flow/04_if.xml");
		ex.put("skip", "/examples/C_flow/05_skip.xml");
		ex.put("every", "/examples/C_flow/06_every.xml");
		ex.put("pull_central", "/examples/C_flow/07_tcp_pull_central.xml");
		ex.put("pull_sources", "/examples/C_flow/07_tcp_pull_sources.xml");
		ex.put("push_central", "/examples/C_flow/08_tcp_push_central.xml");
		ex.put("push_sources", "/examples/C_flow/08_tcp_push_sources.xml");

		// D_properties
		ex.put("prop", "/examples/D_properties/01_properties.xml");

		// E_expressions
		ex.put("exp", "/examples/E_expressions/01_expressions.xml");

		// F_service
		ex.put("service", "/examples/F_service/01_service.xml");
		return ex;
	}

	public static void main(String[] args) throws InterruptedException,
			ExecutionException {

		Containers containers = new Containers(2);

		// start(containers, "push_sources");

		start(containers, "push_central");

		// Map<String, String> ex = getExamples();
		// Containers all = new Containers(ex.size());
		// for (String k : ex.keySet()) {
		// start(all, k);
		// }
	}

	public static void start(Containers containers, String name,
			Map<String, String> props) throws InterruptedException,
			ExecutionException {
		Container c = new Container(
				StartExample.class.getResource(getExamples().get(name)));
		containers.put(name, c);
		Future<Boolean> result = containers.start(name);
		if (result == null)
			System.out.println("Example failed!");
		boolean end = result.get();
		if (end) {
			//containers.stop(name);
			//containers.stop();
		}
	}

	public static void start(Containers containers, String name)
			throws InterruptedException, ExecutionException {
		Map<String, String> props = new HashMap<String, String>();
		start(containers, name, props);
	}
}
