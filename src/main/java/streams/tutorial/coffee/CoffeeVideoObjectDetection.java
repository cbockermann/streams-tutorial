/**
 * 
 */
package streams.tutorial.coffee;

import java.net.URL;

/**
 * @author chris
 * 
 */
public class CoffeeVideoObjectDetection {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		URL url = CoffeeVideoObjectDetection.class
				.getResource("/chapter6/coffee-video-objectDetection.xml");
		stream.run.main(url);
	}

}
