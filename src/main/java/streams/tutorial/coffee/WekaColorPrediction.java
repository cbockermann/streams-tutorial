/**
 * 
 */
package streams.tutorial.coffee;

import java.net.URL;

/**
 * @author chris
 * 
 */
public class WekaColorPrediction {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		URL url = WekaColorPrediction.class
				.getResource("/chapter6/train-weka.xml");
		stream.run.main(url);
	}
}
