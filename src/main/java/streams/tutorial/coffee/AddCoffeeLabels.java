/**
 * 
 */
package streams.tutorial.coffee;

import java.util.LinkedHashMap;
import java.util.Map;

import stream.AbstractProcessor;
import stream.Data;
import stream.ProcessContext;
import stream.io.CsvStream;
import stream.io.SourceURL;
import stream.io.Stream;

/**
 * @author chris
 * 
 */
public class AddCoffeeLabels extends AbstractProcessor {

	final Map<String, String> labels = new LinkedHashMap<String, String>();

	SourceURL url;

	/**
	 * @see stream.AbstractProcessor#init(stream.ProcessContext)
	 */
	@Override
	public void init(ProcessContext ctx) throws Exception {
		super.init(ctx);

		if (url == null) {
			url = new SourceURL("classpath:/coffee/coffee-random-labels.txt");
		}

		if (url != null) {
			Stream stream = new CsvStream(url);
			stream.init();
			Data row = stream.read();
			while (row != null) {
				String id = row.get("@id") + "";
				String label = row.get("@label") + "";
				labels.put(id, label);
				row = stream.read();
			}
			stream.close();
		}
	}

	/**
	 * @see stream.Processor#process(stream.Data)
	 */
	@Override
	public Data process(Data input) {
		String id = input.get("@id") + "";
		String label = labels.get(id);
		if (label != null) {
			input.put("@label", label);
		}
		return input;
	}

	/**
	 * @return the url
	 */
	public SourceURL getUrl() {
		return url;
	}

	/**
	 * @param url
	 *            the url to set
	 */
	public void setUrl(SourceURL url) {
		this.url = url;
	}
}