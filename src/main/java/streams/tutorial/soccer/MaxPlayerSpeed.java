/**
 * 
 */
package streams.tutorial.soccer;

import java.util.HashMap;
import java.util.Map;

import stream.Data;
import stream.Processor;

/**
 * @author chris
 * 
 */
public class MaxPlayerSpeed implements Processor {

	Map<Integer, Double> maxima = new HashMap<Integer, Double>();

	/**
	 * @see stream.Processor#process(stream.Data)
	 */
	@Override
	public Data process(Data input) {

		Integer pid = (Integer) input.get("pid");
		if (pid > 0) {

			Double speed = (Double) input.get("speed");
			if (speed == null) {
				return input;
			}

			Double max = maxima.get(pid);
			if (max == null) {
				max = speed;
			} else {
				max = Math.max(max, speed);
			}

			maxima.put(pid, max);
			input.put("x1", max);
		}

		return input;
	}
}
