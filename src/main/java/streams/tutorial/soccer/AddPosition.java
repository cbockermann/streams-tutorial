/**
 * 
 */
package streams.tutorial.soccer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.AbstractProcessor;
import stream.Data;
import stream.ProcessContext;

/**
 * <p>
 * This process computes the position of player as the center between the latest
 * leg-sensors received for each player and adds a new position object to the
 * item with key "pos".
 * </p>
 * <p>
 * In addition the coordinates x, y and z are added with keys "pos.x", "pos.y"
 * and "pos.z" as Double values.
 * </p>
 * 
 * @author Christian Bockermann
 * 
 */
public class AddPosition extends AbstractProcessor {

	static Logger log = LoggerFactory.getLogger(AddPosition.class);
	// final ArrayList<PositionData> posData = new ArrayList<PositionData>();
	final PositionData[] posData = new PositionData[24];
	// final Map<Integer, PositionData> posData = new HashMap<Integer,
	// PositionData>();

	int gridx = -1;
	int gridy = -1;

	/**
	 * @see stream.AbstractProcessor#init(stream.ProcessContext)
	 */
	@Override
	public void init(ProcessContext ctx) throws Exception {
		super.init(ctx);

		for (int i = 0; i < posData.length; i++) {
			posData[i] = new PositionData();
		}
	}

	/**
	 * @see stream.Processor#process(stream.Data)
	 */
	public Data process(Data input) {

		Integer pid = (Integer) input.get("pid");
		String type = (input.get("type") + "");

		Double x = ((Integer) input.get("x")).doubleValue();
		Double y = ((Integer) input.get("y")).doubleValue();
		Double z = ((Integer) input.get("z")).doubleValue();

		if (pid >= 0 && type.toLowerCase().indexOf("leg") >= 0) {
			PositionData pd = posData[pid];
			pd.add(x, y, z);

			input.put("player.x", pd.x());
			input.put("player.y", pd.y());
			input.put("player.z", pd.z());
		}
		input.put("pos.x", x);
		input.put("pos.y", y);
		input.put("pos.z", z);

		return input;
	}

	static class PositionData {
		double x1, y1, z1;
		double x2, y2, z2;
		int count = 0;

		public PositionData add(double x, double y, double z) {
			if (count % 2 == 0) {
				x1 = x;
				y1 = y;
				z1 = z;
			} else {
				x2 = x;
				y2 = y;
				z2 = z;
			}

			count++;
			return this;
		}

		public double x() {
			if (count < 2)
				return x1;

			return (x1 + x2) / 2.0;
		}

		public double y() {
			if (count < 2)
				return y1;

			return (y1 + y2) / 2.0;
		}

		public double z() {
			if (count < 2)
				return z1;
			return (z1 + z2) / 2.0d;
		}
	}
}
