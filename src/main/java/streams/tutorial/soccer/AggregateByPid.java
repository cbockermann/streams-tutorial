/**
 * 
 */
package streams.tutorial.soccer;

import java.util.HashMap;
import java.util.Map;

import stream.Data;
import stream.Processor;
import stream.data.DataFactory;
import stream.io.Sink;

/**
 * <p>
 * A simple aggregation of the running distance by PID
 * </p>
 * 
 * @author Christian Bockermann
 * 
 */
public class AggregateByPid implements Processor {

	final Map<Integer, Double> agg = new HashMap<Integer, Double>();
	String key = "distance";
	Sink output;

	/**
	 * @see stream.Processor#process(stream.Data)
	 */
	@Override
	public Data process(Data input) {

		Integer pid = (Integer) input.get("pid");
		if (pid < 1) {
			return input;
		}

		Double cur = agg.get(pid);
		if (cur == null) {
			cur = 0.0d;
		}

		Double value = (Double) input.get(key);

		if (value != null) {
			cur += value;
		}

		if (output != null) {
			Data out = DataFactory.create();
			out.put("total:" + key, cur);
			try {
				output.write(out);
			} catch (Exception e) {
			}
		}

		input.put("total:" + key, cur);
		agg.put(pid, cur);
		return input;
	}

	/**
	 * @return the output
	 */
	public Sink getOutput() {
		return output;
	}

	/**
	 * @param output
	 *            the output to set
	 */
	public void setOutput(Sink output) {
		this.output = output;
	}

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @param key
	 *            the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}

}
