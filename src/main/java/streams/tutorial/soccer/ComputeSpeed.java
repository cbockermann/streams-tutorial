/**
 * 
 */
package streams.tutorial.soccer;

import java.util.HashMap;
import java.util.Map;

import stream.Data;
import stream.Processor;

/**
 * @author chris
 * 
 */
public class ComputeSpeed implements Processor {

	Map<Integer, Long> lastTime = new HashMap<Integer, Long>();

	/**
	 * @see stream.Processor#process(stream.Data)
	 */
	@Override
	public Data process(Data input) {

		Integer pid = (Integer) input.get("pid");
		Double meters = (Double) input.get("distance");

		if (pid > 0 && meters != null) {

			Double delta = 1000.0 / 200.0; // Sample rate is 200 Hz

			input.put("speed", meters / delta);

		}

		return input;
	}
}
