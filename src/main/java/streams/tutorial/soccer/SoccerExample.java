/**
 * 
 */
package streams.tutorial.soccer;

import java.net.URL;

/**
 * @author Christian Bockermann
 * 
 */
public class SoccerExample {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {

		// fetch the XML url from the classpath
		//
		URL url = SoccerExample.class
				.getResource("/soccer/soccer-analysis.xml");

		// start the XML using the streams-runtime
		//
		stream.run.main(url);
	}
}
