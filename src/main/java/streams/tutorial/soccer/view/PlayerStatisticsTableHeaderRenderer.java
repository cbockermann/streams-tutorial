/**
 * 
 */
package streams.tutorial.soccer.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/**
 * @author chris
 * 
 */
public class PlayerStatisticsTableHeaderRenderer extends JPanel implements
		TableCellRenderer {

	/** The unique class ID */
	private static final long serialVersionUID = -7534128202908992786L;

	final static Color GREEN = new Color(171, 194, 174);
	final JLabel label = new JLabel();

	public PlayerStatisticsTableHeaderRenderer() {
		setBackground(GREEN);
		setForeground(Color.BLACK);

		setLayout(new BorderLayout());

		label.setFont(new Font("SansSerif", Font.BOLD, 12));
		label.setForeground(Color.BLACK);
		label.setBackground(GREEN);

		this.setBorder(null);
		label.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
		add(label, BorderLayout.CENTER);
	}

	/**
	 * @see javax.swing.table.TableCellRenderer#getTableCellRendererComponent(javax
	 *      .swing.JTable, java.lang.Object, boolean, boolean, int, int)
	 */
	@Override
	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {

		label.setText(value + "");
		return this;
	}

	/**
	 * @see javax.swing.JComponent#getPreferredSize()
	 */
	@Override
	public Dimension getPreferredSize() {
		return new Dimension(32, super.getPreferredSize().width);
	}

}
