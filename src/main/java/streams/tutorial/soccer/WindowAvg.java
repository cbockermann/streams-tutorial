/**
 * 
 */
package streams.tutorial.soccer;

/**
 * @author chris
 * 
 */
public class WindowAvg {

	int updates = 0;
	final double[] values;
	final double divisor;
	double current;

	public WindowAvg(Integer steps) {
		values = new double[steps];
		for (int i = 0; i < values.length; i++) {
			values[i] = 0.0d;
		}
		current = 0;
		divisor = steps.doubleValue();
	}

	public double add(double val) {
		int idx = (++updates % values.length);
		values[idx] = val;
		current -= values[(idx + values.length + 1) % values.length];
		return current / divisor;
	}

	public double value() {
		return current;
	}
}
