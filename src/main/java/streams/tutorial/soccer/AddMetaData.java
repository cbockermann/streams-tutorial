/**
 * 
 */
package streams.tutorial.soccer;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.AbstractProcessor;
import stream.Data;
import stream.ProcessContext;
import stream.data.DataFactory;
import stream.io.SourceURL;

/**
 * <p>
 * This processor will read meta-data from a
 * </p>
 * 
 * @author Christian Bockermann
 * 
 */
public class AddMetaData extends AbstractProcessor {

	static Logger log = LoggerFactory.getLogger(AddMetaData.class);
	String key = "id";
	SourceURL source;
	final Map<Integer, Data> metaData = new HashMap<Integer, Data>();
	String[] fields = "id,pid,type".split(",");

	public AddMetaData() {
		try {
			source = new SourceURL("classpath:/soccer/metadata.json");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @param key
	 *            the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * @return the source
	 */
	public SourceURL getSource() {
		return source;
	}

	/**
	 * @param source
	 *            the source to set
	 */
	public void setSource(SourceURL source) {
		this.source = source;
	}

	/**
	 * @see stream.AbstractProcessor#init(stream.ProcessContext)
	 */
	@Override
	public void init(ProcessContext ctx) throws Exception {
		super.init(ctx);

		final JSONParser p = new JSONParser(JSONParser.MODE_PERMISSIVE);
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				source.openStream()));
		String line = reader.readLine();
		while (line != null) {

			Data meta = DataFactory.create();
			JSONObject obj = (JSONObject) p.parse(line);
			for (String key : obj.keySet()) {

				if (!"id".equals(key) && !contains(key)) {
					log.debug("Skipping meta-data field '{}'", key);
					continue;
				}

				if (key.equals("type")) {

					if (obj.get(key).toString().equals("left leg")) {
						meta.put(key, "LeftLeg");
						meta.put("leg", 0);
						continue;
					}

					if (obj.get(key).toString().equals("right leg")) {
						meta.put(key, "RightLeg");
						meta.put("leg", 1);
						continue;
					}

					if (obj.get(key).toString().indexOf("all") >= 0)
						meta.put(key, "Ball");
				} else {
					try {
						meta.put(key, new Integer(obj.get(key) + ""));
					} catch (Exception e) {
						meta.put(key, (Serializable) obj.get(key));
					}
				}
			}
			log.debug("Adding {}", meta);

			Integer k = new Integer(meta.get(key).toString());
			if (k != null) {
				log.debug("Adding meta-data for key '{}'", k);

				metaData.put(k, meta);
			}

			line = reader.readLine();
		}

		reader.close();
	}

	/**
	 * @see stream.Processor#process(stream.Data)
	 */
	public Data process(Data input) {

		if (input == null)
			return input;

		Integer id = (Integer) input.get(key);
		if (id == null) {
			return input;
		}

		Data meta = metaData.get(id);
		if (meta != null) {
			input.putAll(meta);
		}

		return input;
	}

	protected boolean contains(String f) {
		if (fields == null)
			return true;

		for (String field : fields) {
			if (field.equals(f))
				return true;
		}
		return false;
	}
}
