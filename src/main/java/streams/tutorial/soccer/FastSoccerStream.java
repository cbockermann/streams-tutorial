/**
 * 
 */
package streams.tutorial.soccer;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

import stream.Data;
import stream.data.DataFactory;
import stream.io.AbstractStream;
import stream.io.SourceURL;

/**
 * <p>
 * A specific stream implementation to the DEBS 2013 challenge data.
 * </p>
 * 
 * @author Christian Bockermann
 * 
 */
public class FastSoccerStream extends AbstractStream {

	public final static String[] KEYS = "id,timestamp,x,y,z,v,a,vx,vy,vz,ax,ay,az"
			.split(",");
	private BufferedReader reader = null;

	int blockSize = -1;
	ArrayList<Data> block = new ArrayList<Data>();

	public FastSoccerStream(SourceURL url) {
		super(url);
	}

	/**
	 * @see stream.io.AbstractStream#init()
	 */
	@Override
	public void init() throws Exception {
		super.init();
		reader = new BufferedReader(
				new InputStreamReader(this.getInputStream()));
		if (blockSize > 0) {
			block = new ArrayList<Data>(blockSize);
		}
	}

	/**
	 * @return the blockSize
	 */
	public int getBlockSize() {
		return blockSize;
	}

	/**
	 * @param blockSize
	 *            the blockSize to set
	 */
	public void setBlockSize(int blockSize) {
		this.blockSize = blockSize;
	}

	/**
	 * @see stream.io.AbstractStream#readNext()
	 */
	@Override
	public Data readNext() throws Exception {

		if (blockSize <= 0) {
			return readItem();
		}

		if (block.isEmpty()) {
			int read = readBlock();
			if (read == 0)
				return null;
		}

		Data item = block.remove(0);
		return item;
	}

	protected int readBlock() throws Exception {
		block.clear();
		int read = 0;

		for (int i = 0; i < blockSize; i++) {
			Data item = readItem();
			if (item != null) {
				block.add(item);
				read++;
			} else {
				return read;
			}
		}
		return read;
	}

	protected Data readItem() throws Exception {

		String line = reader.readLine();
		if (line == null)
			return null;

		final Data item = DataFactory.create();
		int i = 0;
		int len = line.length();
		int max = len - 1;
		int sign = 1;

		Long longVal = 0L;

		for (int c = 0; c < len; c++) {
			char ch = line.charAt(c);

			if (ch == '-') {
				// log.info("Reading negative number for key {}, line[" + c
				// + "]: '{}'", KEYS[i], ch);
				sign = -1;
				continue;
			}

			if (ch == ',') {
				if (i == 1) {
					if (sign == -1)
						item.put(KEYS[i], -longVal);
					else
						item.put(KEYS[i], longVal);
				} else {
					if (sign == -1)
						item.put(KEYS[i], -longVal.intValue());
					else
						item.put(KEYS[i], longVal.intValue());
				}
				longVal = 0L;
				sign = 1;
				i++;

				if (ch == '\n')
					return item;
				continue;
			}

			longVal *= 10;
			longVal += (ch - '0');

			if (c == max) {
				if (sign == -1)
					item.put(KEYS[i], -longVal.intValue());
				else
					item.put(KEYS[i], longVal.intValue());
				return item;
			}
		}

		return item;
	}
}
