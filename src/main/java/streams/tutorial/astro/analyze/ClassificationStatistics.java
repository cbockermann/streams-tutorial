package streams.tutorial.astro.analyze;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.AbstractProcessor;
import stream.Data;
import stream.ProcessContext;
import streams.tutorial.astro.PrintOutput;

/**
 * Created by alexey on 30.09.14.
 * http://en.wikipedia.org/wiki/Sensitivity_and_specificity
 */
public class ClassificationStatistics extends AbstractProcessor {

    final static Logger log = LoggerFactory.getLogger(ClassificationStatistics.class);
    public static int

            // true positive: event detected while there was a real event there
            truePositive = 0,

            // false negative: no event detected while there was an event there
            falseNegative = 0,

            // true negative: no event found and no event really detected on picture
            trueNegative = 0,

            // false positive: event detected where there was an event
            falsePositive = 0,

            // true positive: event detected while there was a real event there
            truePositiveWorm = 0,

            // false negative: no event detected while there was an event there
            falseNegativeWorm = 0,

            // true negative: no event found and no event really detected on picture
            trueNegativeWorm = 0,

            // false positive: event detected where there was an event
            falsePositiveWorm = 0;

    @Override
    public void init(ProcessContext ctx) throws Exception {
        super.init(ctx);
        truePositive = 0;
        falseNegative = 0;
        trueNegative = 0;
        falsePositive = 0;
        truePositiveWorm = 0;
        falseNegativeWorm = 0;
        trueNegativeWorm = 0;
        falsePositiveWorm = 0;
    }

    @Override
    public Data process(Data item) {
        if (!item.containsKey("detected")){
            return null;
        }

        Boolean detected = (Boolean) item.get("detected");
        Integer worm =
                item.containsKey("worm") ?
                        (Integer) item.get("worm") : 0;
        Integer x1 =
                item.containsKey("x1") ?
                        (Integer) item.get("x1") : -1;
        Boolean worm_detected =
                item.containsKey("worm_detected") ?
                        (detected ? (Boolean) item.get("worm_detected") : false) : false;

        if (x1 != -1) {
            // there is an event in the image
            // as we're in this class, then we detected something
            if (detected){
                ClassificationStatistics.truePositive++;
                updateWormStatistics(worm, worm_detected);
            }else
                ClassificationStatistics.falseNegative++;
        }else{
            // there is no event in this image
            // but we detected something!
            if (detected) {
                ClassificationStatistics.falsePositive++;
                updateWormStatistics(worm, worm_detected);
            }else
                ClassificationStatistics.trueNegative++;
        }

        String message = "Statistics:\n"
                + "Acc: " + String.format("%.2f", calculateAccuracy()) + "\t"
                + "Sens: " + String.format("%.2f", calculateSensitivity()) + "\t"
                + "Prec: " + String.format("%.2f", calculatePrecision()) + "\n"
                + "WormAcc: " + String.format("%.2f", calculateAccuracyWorm()) + "\t"
                + "WormSens: " + String.format("%.2f", calculateSensitivityWorm()) + "\t"
                + "WormPrec: " + calculatePrecisionWorm() + "\n";
        log.info(message);

        String output = (String) item.get(PrintOutput.PRINT_OUTPUT);
        output = (output != null ? output : "");
        if (detected) {
            output += message;
        }else{
            output += "Nothing detected.\n" + message;
        }
        item.put(PrintOutput.PRINT_OUTPUT, output);
        return item;
    }

    private void updateWormStatistics(int worm, boolean worm_detected){
        if (worm==1){
            if (worm_detected){
                ClassificationStatistics.truePositiveWorm++;
            }else{
                ClassificationStatistics.falseNegativeWorm++;
            }
        }else{
            if(worm_detected){
                ClassificationStatistics.falsePositiveWorm++;
            }else{
                ClassificationStatistics.trueNegativeWorm++;
            }
        }
    }

    private static double calculatePrecision(){
        double divide = truePositive + falsePositive;
        return divide==0.0 ? 1 : truePositive / divide;
    }

    private static double calculateSensitivity(){
        double divide = truePositive + falseNegative;
        return divide==0.0 ? 1 : truePositive / divide;
    }

    private static double calculateAccuracy(){
        double divide = truePositive + falseNegative + trueNegative + falsePositive;
        return divide==0.0 ? 1 : (truePositive + trueNegative)/ divide;
    }

    private static double calculatePrecisionWorm(){
        double divide = truePositiveWorm + falsePositiveWorm;
        return divide==0.0 ? 1 : truePositiveWorm / divide;
    }

    private static double calculateSensitivityWorm(){
        double divide = truePositiveWorm + falseNegativeWorm;
        return divide==0.0 ? 1 : truePositiveWorm / divide;
    }

    private static double calculateAccuracyWorm(){
        double divide = truePositiveWorm + falseNegativeWorm + trueNegativeWorm + falsePositiveWorm;
        return divide==0.0 ? 1 : (truePositiveWorm + trueNegativeWorm)/ divide;
    }
}
