/**
 * 
 */
package streams.example;

import java.io.Serializable;

import stream.Data;
import stream.data.DataFactory;
import stream.flow.Emitter;

/**
 * @author chris
 * 
 */
public class TagExtractor extends Emitter {

	/**
	 * @see stream.Processor#process(stream.Data)
	 */
	@Override
	public Data process(Data input) {

		Serializable value = input.get("text");
		if (value != null) {

			for (String word : value.toString().split("\\s+")) {

				if (word.startsWith("#")) {
					Data item = DataFactory.create();
					item.put("tag", word.toLowerCase());

					emit(item);
				}
			}
		}

		return input;
	}
}
