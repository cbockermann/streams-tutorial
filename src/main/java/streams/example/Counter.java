/**
 * 
 */
package streams.example;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import stream.Data;
import stream.data.DataFactory;
import stream.flow.Emitter;

/**
 * @author chris
 * 
 */
public class Counter extends Emitter {

	final Map<String, Integer> counts = new HashMap<String, Integer>();

	String key = "tag";

	Integer threshold = 5;

	/**
	 * @see stream.Processor#process(stream.Data)
	 */
	@Override
	public Data process(Data input) {

		Serializable value = input.get(key);
		if (value != null) {

			String tag = value.toString();

			Integer cnt = 1;
			if (counts.containsKey(tag)) {
				cnt = counts.get(tag) + 1;
			}

			counts.put(tag, cnt);

			if (cnt > threshold) {
				Data out = DataFactory.create();
				out.put(tag, cnt);
				emit(out);
			}
		}

		return input;
	}

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @param key
	 *            the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * @return the threshold
	 */
	public Integer getThreshold() {
		return threshold;
	}

	/**
	 * @param threshold
	 *            the threshold to set
	 */
	public void setThreshold(Integer threshold) {
		this.threshold = threshold;
	}
}
