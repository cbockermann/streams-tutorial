/**
 * 
 */
package streams.example;

import java.net.URL;

/**
 * @author chris
 * 
 */
public class TagExampleOnStorm {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		URL url = TagExampleOnStorm.class
				.getResource("/examples/G_storm/tag-counting-streams.xml");
		storm.run.main(url, 10000L);
	}
}
