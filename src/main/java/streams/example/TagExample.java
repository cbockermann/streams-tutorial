/**
 * 
 */
package streams.example;

import java.net.URL;

/**
 * @author chris
 * 
 */
public class TagExample {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		URL url = TagExample.class.getResource("/tag-counting-streams.xml");
		stream.run.main(url);
	}
}
